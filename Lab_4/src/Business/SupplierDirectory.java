/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import java.util.ArrayList;
/**
 *
 * @author Reyes
 */
public class SupplierDirectory {
    private ArrayList<Supplier> sd;
    
    public SupplierDirectory()
    {
        sd = new ArrayList<Supplier>();
    }

    public ArrayList<Supplier> getSd() {
        return sd;
    }
    
    public Supplier addSupplier()
    {
        Supplier s = new Supplier();
        sd.add(s);
        return s;
    }
    
    public void deleteSupplier(Supplier s)
    {
        sd.remove(s);
    }
    
    public Supplier searchSupplier(String name)
    {
        for (Supplier s : sd)
            if (s.getSuppliername().equals(name))
                return s;
        return null;
    }
}
