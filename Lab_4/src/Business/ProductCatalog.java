/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import java.util.ArrayList;
/**
 *
 * @author Reyes
 */
public class ProductCatalog {
    private ArrayList<Product> pc;

    public ProductCatalog() {
        pc = new ArrayList<Product>();
    }

    public ArrayList<Product> getPc() {
        return pc;
    }
    
   public Product addProduct()
   {
       Product p = new Product();
       pc.add(p);
       return p;
   }
   
   public void deleteProduct(Product p)
   {
       pc.remove(p);
   }
   
   public Product searchProduct(int id)
   {
       for (Product p : pc)
           if (p.getModel() == id)
               return p;
       return null;       
   }
           


}
