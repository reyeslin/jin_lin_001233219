/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Reyes
 */
public class Product {
     private int price;
     private String proname;
     private int model;
     private static int count = 0;

    public Product() {
        count++;
        model = count;
    }
    @Override
    public String toString()
    {
        return this.proname;
    }
            
    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getProname() {
        return proname;
    }

    public void setProname(String proname) {
        this.proname = proname;
    }

    public int getModel() {
        return model;
    }

    public void setModel(int model) {
        this.model = model;
    }    
    
     
}
