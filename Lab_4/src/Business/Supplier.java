/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Reyes
 */
public class Supplier {
    private String suppliername;
    private ProductCatalog pc;
    
    public Supplier()
    {
        pc = new ProductCatalog();       
    }

    public String getSuppliername() {
        return suppliername;
    }

    public void setSuppliername(String suppliername) {
        this.suppliername = suppliername;
    }

    public ProductCatalog getPc() {
        return pc;
    }
    
    @Override
    public String toString()
    {
        return this.suppliername;
    }

    
}
