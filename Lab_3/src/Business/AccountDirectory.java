/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import java.util.ArrayList;
/**
 *
 * @author Reyes
 */
public class AccountDirectory {
    private ArrayList<Account> ad;

    public AccountDirectory() {
        ad = new ArrayList<Account>();
    }

    public ArrayList<Account> getAd() {
        return ad;
    }

    public void setAd(ArrayList<Account> ad) {
        this.ad = ad;
    }
      
    public Account add()
    {
        Account ac = new Account();
        ad.add(ac);
        return ac;
    }
    
    public void Delete(Account ac)
    {
         ad.remove(ac);
    }
    
    public Account search(String accNum)
    {
        for (Account ac : ad)
            if (ac.getAccountNumber().equals(accNum))
                return ac;
        return null;        
    }
}
