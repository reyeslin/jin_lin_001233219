/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;
import Airplane.Airplanes;
import Airplane.FleetCatalog;
import java.io.BufferedReader;
import java.io.*;
import java.util.Random;
import javax.swing.JOptionPane;
/**
 *
 * @author Reyes
 */
public class MainJFrame extends javax.swing.JFrame {    
    private FleetCatalog fc;
    public MainJFrame() {
        initComponents();
        fc = new FleetCatalog();
        
        
    }
    
    public void ReadFile()
    {
         try {  
            File file = new File("hw2.csv");
            BufferedReader bufread;  
            String read;  
            bufread = new BufferedReader(new FileReader(file));  
            while ((read = bufread.readLine()) != null) {  
                Savedata(read);  
            }  
            bufread.close();  
        } catch (FileNotFoundException ex) {  
            ex.printStackTrace();  
        } catch (IOException ex) {  
            ex.printStackTrace();  
        }  
    }
    public void Savedata(String st)
    {
        Airplanes ap = fc.addairplane();
        int index=0;
        String s="";
        //ID
        while (st.charAt(index)!=',')
        {s=s+st.charAt(index);        
        index++;        
        }        
        ap.setId(s); 
                //L_D
        index++;
        s="";
        while (st.charAt(index)!=',')
        {s=s+st.charAt(index);        
        index++;        
        }        
        ap.setLeave_date(Integer.parseInt(s)); 
        //L_T
        index++;
        s="";
        while (st.charAt(index)!=',')
        {s=s+st.charAt(index);        
        index++;        
        }        
        ap.setLeave_time(Integer.parseInt(s)); 
        //L_A
        index++;
        s="";
        while (st.charAt(index)!=',')
        {s=s+st.charAt(index);        
        index++;        
        }        
        ap.setLeave_airport(s);
        //A_D
        index++;
        s="";
        while (st.charAt(index)!=',')
        {s=s+st.charAt(index);        
        index++;        
        }        
        ap.setArrive_date(Integer.parseInt(s)); 
        //A_T
        index++;
        s="";
        while (st.charAt(index)!=',')
        {s=s+st.charAt(index);        
        index++;        
        }        
        ap.setArrive_time(Integer.parseInt(s)); 
        //A_A
        index++;
        s="";
        while (st.charAt(index)!=',')
        {s=s+st.charAt(index);        
        index++;        
        }        
        ap.setArrive_airport(s);

        
        //Company
        index++;
        s="";
        while (st.charAt(index)!=',')
        {s=s+st.charAt(index);        
        index++;        
        }   
        ap.setCompany(s);
        //Year
        index++;
        s="";
        while (st.charAt(index)!=',')
        {s=s+st.charAt(index);        
        index++;       
        }               
        ap.setYear(Integer.parseInt(s));
        //Seats
        index++;
        s="";
        while (st.charAt(index)!=',')
        {s=s+st.charAt(index);        
        index++;       
        }   
        ap.setSeats(Integer.parseInt(s));
        //Serial
        index++;
        s="";
        while (st.charAt(index)!=',')
        {s=s+st.charAt(index);        
        index++;       
        }  
        ap.setSerial(s);
        //Model
        index++;
        s="";
        while (st.charAt(index)!=',')
        {s=s+st.charAt(index);        
        index++;       
        }  
        ap.setModel(Integer.parseInt(s));
        //Manufacture
        index++;
        s="";
        while (st.charAt(index)!=',')
        {s=s+st.charAt(index);        
        index++;       
        }  
        ap.setManufacturer(s);
        //EMC
        index++;                
        if (st.charAt(index)=='0') ap.setExpire(false); else ap.setExpire(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Split = new javax.swing.JSplitPane();
        ControlPanel = new javax.swing.JPanel();
        FirstPlaneBtn = new javax.swing.JButton();
        AvailabilityBtn = new javax.swing.JButton();
        BoeingBtn = new javax.swing.JButton();
        YearBtn = new javax.swing.JButton();
        SeatBtn = new javax.swing.JButton();
        SerialBtn = new javax.swing.JButton();
        ModelBtn = new javax.swing.JButton();
        ManufacturerBtn = new javax.swing.JButton();
        LastUpdateBtn = new javax.swing.JButton();
        AirportBtn = new javax.swing.JButton();
        ExpireBtn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        CreateBtn = new javax.swing.JButton();
        UpdateBtn = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        DisplayPanel = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txt = new javax.swing.JTextField();
        Create = new javax.swing.JButton();
        read = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        FirstPlaneBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        FirstPlaneBtn.setText("First Available Airplane");
        FirstPlaneBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FirstPlaneBtnActionPerformed(evt);
            }
        });

        AvailabilityBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        AvailabilityBtn.setText("Available & Not available Airplane");
        AvailabilityBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AvailabilityBtnActionPerformed(evt);
            }
        });

        BoeingBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        BoeingBtn.setText("Made By Boeing");
        BoeingBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BoeingBtnActionPerformed(evt);
            }
        });

        YearBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        YearBtn.setText("Productive Year");
        YearBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                YearBtnActionPerformed(evt);
            }
        });

        SeatBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        SeatBtn.setText("Seats Range");
        SeatBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SeatBtnActionPerformed(evt);
            }
        });

        SerialBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        SerialBtn.setText("Serial Number");
        SerialBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SerialBtnActionPerformed(evt);
            }
        });

        ModelBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        ModelBtn.setText("Model Number");
        ModelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ModelBtnActionPerformed(evt);
            }
        });

        ManufacturerBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        ManufacturerBtn.setText("View Manufacturers");
        ManufacturerBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ManufacturerBtnActionPerformed(evt);
            }
        });

        LastUpdateBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LastUpdateBtn.setText("Last time Update Fleet Catalog");
        LastUpdateBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LastUpdateBtnActionPerformed(evt);
            }
        });

        AirportBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        AirportBtn.setText("All Airplains in a Given Airport");
        AirportBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AirportBtnActionPerformed(evt);
            }
        });

        ExpireBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        ExpireBtn.setText("Expired Maintenance Certificate");
        ExpireBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ExpireBtnActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Sitka Text", 1, 24)); // NOI18N
        jLabel1.setText("Find Airplanes");

        jLabel2.setFont(new java.awt.Font("Sitka Text", 1, 24)); // NOI18N
        jLabel2.setText("Operation");

        CreateBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        CreateBtn.setText("Create Airplane");
        CreateBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CreateBtnActionPerformed(evt);
            }
        });

        UpdateBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        UpdateBtn.setText("Update Airplane");
        UpdateBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UpdateBtnActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton1.setText("Set Current Time");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout ControlPanelLayout = new javax.swing.GroupLayout(ControlPanel);
        ControlPanel.setLayout(ControlPanelLayout);
        ControlPanelLayout.setHorizontalGroup(
            ControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ControlPanelLayout.createSequentialGroup()
                .addGroup(ControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ControlPanelLayout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(ControlPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(ControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(ControlPanelLayout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addComponent(jLabel2)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(UpdateBtn, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(CreateBtn, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(FirstPlaneBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(AvailabilityBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(BoeingBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(YearBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(SeatBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(SerialBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(ModelBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(ManufacturerBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(LastUpdateBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(AirportBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(ExpireBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        ControlPanelLayout.setVerticalGroup(
            ControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ControlPanelLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(FirstPlaneBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(AvailabilityBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BoeingBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(YearBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(SeatBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(SerialBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ModelBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ManufacturerBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LastUpdateBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(AirportBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ExpireBtn)
                .addGap(35, 35, 35)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CreateBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(UpdateBtn)
                .addContainerGap(219, Short.MAX_VALUE))
        );

        Split.setLeftComponent(ControlPanel);

        jLabel3.setFont(new java.awt.Font("Sitka Text", 1, 18)); // NOI18N
        jLabel3.setText("How many airplanes do you want to create randomly into .csv file?");

        txt.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        Create.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Create.setText("Create");
        Create.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CreateActionPerformed(evt);
            }
        });

        read.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        read.setText("Read File");
        read.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                readActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout DisplayPanelLayout = new javax.swing.GroupLayout(DisplayPanel);
        DisplayPanel.setLayout(DisplayPanelLayout);
        DisplayPanelLayout.setHorizontalGroup(
            DisplayPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DisplayPanelLayout.createSequentialGroup()
                .addGroup(DisplayPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(DisplayPanelLayout.createSequentialGroup()
                        .addGap(268, 268, 268)
                        .addComponent(txt, javax.swing.GroupLayout.PREFERRED_SIZE, 263, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(DisplayPanelLayout.createSequentialGroup()
                        .addGap(362, 362, 362)
                        .addComponent(Create, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(102, 102, 102)
                        .addComponent(read))
                    .addGroup(DisplayPanelLayout.createSequentialGroup()
                        .addGap(126, 126, 126)
                        .addComponent(jLabel3)))
                .addContainerGap(166, Short.MAX_VALUE))
        );
        DisplayPanelLayout.setVerticalGroup(
            DisplayPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DisplayPanelLayout.createSequentialGroup()
                .addGap(179, 179, 179)
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addComponent(txt, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(DisplayPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Create)
                    .addComponent(read, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(454, Short.MAX_VALUE))
        );

        Split.setRightComponent(DisplayPanel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Split)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Split)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void FirstPlaneBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FirstPlaneBtnActionPerformed
        FirstAirplane fa = new FirstAirplane(fc);
        Split.setRightComponent(fa);
    }//GEN-LAST:event_FirstPlaneBtnActionPerformed

    private void SeatBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SeatBtnActionPerformed
        Seats seats = new Seats(fc);
        Split.setRightComponent(seats);
    }//GEN-LAST:event_SeatBtnActionPerformed

    private void CreateBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CreateBtnActionPerformed
        CreateAirplane ca = new CreateAirplane(fc);
        Split.setRightComponent(ca);
    }//GEN-LAST:event_CreateBtnActionPerformed

    private void AvailabilityBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AvailabilityBtnActionPerformed
        Availability av = new Availability(fc);
        Split.setRightComponent(av);
    }//GEN-LAST:event_AvailabilityBtnActionPerformed

    private void BoeingBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BoeingBtnActionPerformed
        Boeing boeing = new Boeing(fc);
        Split.setRightComponent(boeing);
    }//GEN-LAST:event_BoeingBtnActionPerformed

    private void YearBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_YearBtnActionPerformed
        Year year = new Year(fc);
        Split.setRightComponent(year);
    }//GEN-LAST:event_YearBtnActionPerformed

    private void SerialBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SerialBtnActionPerformed
        Serial serial = new Serial(fc);
        Split.setRightComponent(serial);
    }//GEN-LAST:event_SerialBtnActionPerformed

    private void ModelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ModelBtnActionPerformed
        Model model = new Model (fc);
        Split.setRightComponent(model);
    }//GEN-LAST:event_ModelBtnActionPerformed

    private void ManufacturerBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ManufacturerBtnActionPerformed
        Manufacturer manu = new Manufacturer(fc);
        Split.setRightComponent(manu);
    }//GEN-LAST:event_ManufacturerBtnActionPerformed

    private void LastUpdateBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LastUpdateBtnActionPerformed
        Update update = new Update(fc);
        Split.setRightComponent(update);
    }//GEN-LAST:event_LastUpdateBtnActionPerformed

    private void AirportBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AirportBtnActionPerformed
        Airport ap = new Airport(fc);
        Split.setRightComponent(ap);
    }//GEN-LAST:event_AirportBtnActionPerformed

    private void ExpireBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ExpireBtnActionPerformed
        EMC emc = new EMC(fc);
        Split.setRightComponent(emc);
    }//GEN-LAST:event_ExpireBtnActionPerformed

    private void UpdateBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UpdateBtnActionPerformed
        UpdateAirplane ua = new UpdateAirplane(fc);
        Split.setRightComponent(ua);
    }//GEN-LAST:event_UpdateBtnActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        SetCurrentTime sct = new SetCurrentTime(fc);
        Split.setRightComponent(sct);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void CreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CreateActionPerformed
         try {  
            int i; int a,b,c,d,s;
            int num = Integer.parseInt(txt.getText());
            String[] st = {"Piper", "Bell", "Blackburn", "Grob","Kamen","Sikorsky","McDonnell","Cirrus"};
            String content = "This is the content to write into file";  
            File file = new File("hw2.csv");             
            FileWriter fw = new FileWriter(file);  
            BufferedWriter bw = new BufferedWriter(fw);  
            Random r = new Random();
            for (i=0; i<num; i++)
            {
            bw.write("0"+String.valueOf(i)+",");
            a= r.nextInt(20)+80;
            b= r.nextInt(12)+1;
            c= r.nextInt(30)+1;
            d= a*10000+b*100+c;
            bw.write(String.valueOf(d)+",");
            a= r.nextInt(24);
            b= r.nextInt(60);
            c= r.nextInt(60); bw.write(String.valueOf(a*10000+b*100+c)+",");
            s= r.nextInt(2);
            if (s==0) bw.write("Boston,"); else bw.write("NewYork,");
            bw.write(String.valueOf(d)+",");
            a= r.nextInt(24-a)+a;
            b= r.nextInt(60-b)+b;
            c= r.nextInt(60-c)+c; bw.write(String.valueOf(a*10000+b*100+c)+",");
            s= r.nextInt(2);
            if (s==0) bw.write("Beijing,"); else bw.write("HongKong,");
            
            if (s==0) bw.write("Boeing,"); else bw.write("Ast,");
            s = r.nextInt(120)+1900; bw.write(String.valueOf(s)+",");
            s = r.nextInt(250)+100; bw.write(String.valueOf(s)+",");
            s = r.nextInt(1000000)+100; bw.write(String.valueOf(s)+",");
            s = r.nextInt(100); bw.write(String.valueOf(s)+",");
            s = r.nextInt(8); bw.write(st[s]+",");
           
            //s = r.nextInt(5); if (s==0) bw.write("Logan,"); else bw.write("pdf,");
            s = r.nextInt(2); if (s==0) bw.write("0"); else bw.write("1");
            bw.write("\r\n");
            }
              
            bw.flush();  
            bw.close();  
  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        JOptionPane.showMessageDialog(null, "Create successfully.");
    }//GEN-LAST:event_CreateActionPerformed

    private void readActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_readActionPerformed
        ReadFile();
        JOptionPane.showMessageDialog(null, "Read file successfully.");
    }//GEN-LAST:event_readActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainJFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AirportBtn;
    private javax.swing.JButton AvailabilityBtn;
    private javax.swing.JButton BoeingBtn;
    private javax.swing.JPanel ControlPanel;
    private javax.swing.JButton Create;
    private javax.swing.JButton CreateBtn;
    private javax.swing.JPanel DisplayPanel;
    private javax.swing.JButton ExpireBtn;
    private javax.swing.JButton FirstPlaneBtn;
    private javax.swing.JButton LastUpdateBtn;
    private javax.swing.JButton ManufacturerBtn;
    private javax.swing.JButton ModelBtn;
    private javax.swing.JButton SeatBtn;
    private javax.swing.JButton SerialBtn;
    private javax.swing.JSplitPane Split;
    private javax.swing.JButton UpdateBtn;
    private javax.swing.JButton YearBtn;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JButton read;
    private javax.swing.JTextField txt;
    // End of variables declaration//GEN-END:variables
}
