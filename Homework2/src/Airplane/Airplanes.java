/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Airplane;

/**
 *
 * @author Reyes
 */
public class Airplanes {
    private String id;
    //private boolean availability;
    
    private int leave_date;
    private int leave_time;
    private String leave_airport;
    private int arrive_date;
    private int arrive_time;
    private String arrive_airport;
    
    
    private String company;    
    private String serial;
    private String manufacturer;
    //private String update;
    //private String airport;
    private int year;
    private int seats;
    private int model;
    
    private boolean expire;

    @Override
    public String toString()
    {
        return this.id;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    

    public int getLeave_date() {
        return leave_date;
    }

    public void setLeave_date(int leave_date) {
        this.leave_date = leave_date;
    }

    public int getLeave_time() {
        return leave_time;
    }

    public void setLeave_time(int leave_time) {
        this.leave_time = leave_time;
    }

    public String getLeave_airport() {
        return leave_airport;
    }

    public void setLeave_airport(String leave_airport) {
        this.leave_airport = leave_airport;
    }

    public int getArrive_date() {
        return arrive_date;
    }

    public void setArrive_date(int arrive_date) {
        this.arrive_date = arrive_date;
    }

    public int getArrive_time() {
        return arrive_time;
    }

    public void setArrive_time(int arrive_time) {
        this.arrive_time = arrive_time;
    }

    public String getArrive_airport() {
        return arrive_airport;
    }

    public void setArrive_airport(String arrive_airport) {
        this.arrive_airport = arrive_airport;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public int getModel() {
        return model;
    }

    public void setModel(int model) {
        this.model = model;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

   

    /*public String getAirport() {
        return airport;
    }

    public void setAirport(String airport) {
        this.airport = airport;
    }*/

    public boolean isExpire() {
        return expire;
    }

    public void setExpire(boolean expire) {
        this.expire = expire;
    }
    
}
