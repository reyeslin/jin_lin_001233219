/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Airplane;
import java.util.ArrayList;
/**
 *
 * @author Reyes
 */
public class FleetCatalog {
    private ArrayList<Airplanes>  fc;
    private int time;
    private int date;
    private String lastupdate;
    
    public FleetCatalog()
    { 
        fc = new ArrayList<Airplanes>();
        this.time=0;
        this.date=0;
        lastupdate="";
    }

    public ArrayList<Airplanes> getFc() {
        return fc;
    }

    public void setFc(ArrayList<Airplanes> fc) {
        this.fc = fc;
    }

    public String getLastupdate() {
        return lastupdate;
    }

    public void setLastupdate(String lastupdate) {
        this.lastupdate = lastupdate;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }
    
    public Airplanes addairplane()
    {
        Airplanes ap = new Airplanes();
        fc.add(ap);
        return ap;        
    }
    
    public int getsize()
    {
        return fc.size();
    }
    
    public Airplanes getairplane(int num)
    { 
        return fc.get(num);
    }
}
