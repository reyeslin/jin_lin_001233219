/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.Search;

import Business.*;
import java.awt.CardLayout;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Reyes
 */
public class SearchFlight extends javax.swing.JPanel {
    private JPanel Display;
    private TravelAgency ta;
    private AirlinerDirectory ad;
    private CustomerDirectory cd;
    private String[] constrain;
    private ArrayList<Flight> suit;

    SearchFlight(JPanel Display, TravelAgency ta, String[] constrain) {
        initComponents(); 
        this.Display = Display;
        this.ta = ta;
        ad = ta.getAd();
        cd = ta.getCd();
        this.constrain = constrain;
        String date = "Date:     From      " + constrain[0] +"   To  "+ constrain[1];
        String time = "Preferred day of time:   "+constrain[2];
        String location = "Location:    From     " + constrain[3] + "       to   " + constrain[4];       
       
        Datelb.setText(date);
        if (!constrain[2].equals(""))
        Timelb.setText(time);
        else Timelb.setText("");
        localb.setText(location);
        
        Search();
        poptable();
    }
    
    public void Search()
    {
        suit = new ArrayList<Flight>();
        int i = 0;
        for (Airliner al: ad.getAd())
            for (Flight fl: al.getAl())
                suit.add(fl);
        
        if (!constrain[0].equals(""))        
            while (i<suit.size())
            {
                Flight fl = suit.get(i);
                if (Integer.parseInt(constrain[0])>fl.getDate())
                    suit.remove(fl);
                else i++;
            }                
        i=0;
        if (!constrain[1].equals(""))
            while (i<suit.size())
            {
                Flight fl = suit.get(i);
                if (Integer.parseInt(constrain[1])<fl.getDate())
                    suit.remove(fl);
                else i++;
            }
        
        i=0;
        if (!constrain[2].equals(""))
            while (i<suit.size())
            {
                Flight fl = suit.get(i);
                if (!constrain[2].equals(fl.getTime()))
                    suit.remove(fl);
                else i++;
            }        
        i=0;
        if (!constrain[3].equals(""))
            while (i<suit.size())
            {
                Flight fl = suit.get(i);
                if (!constrain[3].equals(fl.getFromlocation()))
                    suit.remove(fl);
                else i++;
            }         
        i=0;
        if (!constrain[4].equals(""))
            while (i<suit.size())
            {
                Flight fl = suit.get(i);
                if (!constrain[4].equals(fl.getTolocation()))
                    suit.remove(fl);
                else i++;
            }       
    }
    
    public void poptable()
    {
         DefaultTableModel dtm = (DefaultTableModel) Fltbl.getModel();
        dtm.setRowCount(0);
        for (Flight fl:suit.subList(0, suit.size()))           
                {
                    Object[] row = new Object[2];
                    row[0] = fl;                    
                    row[1] = fl.getAvailnum();
                    dtm.addRow(row);
                }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        Fltbl = new javax.swing.JTable();
        Datelb = new javax.swing.JLabel();
        localb = new javax.swing.JLabel();
        Backbtn = new javax.swing.JButton();
        Selectbtn = new javax.swing.JButton();
        Timelb = new javax.swing.JLabel();

        Fltbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Flight ID", "Available seats"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(Fltbl);
        if (Fltbl.getColumnModel().getColumnCount() > 0) {
            Fltbl.getColumnModel().getColumn(0).setResizable(false);
            Fltbl.getColumnModel().getColumn(1).setResizable(false);
        }

        Datelb.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        Datelb.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Datelb.setText("Flights date");

        localb.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        localb.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        localb.setText("Flights location");

        Backbtn.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        Backbtn.setText("<<Back");
        Backbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BackbtnActionPerformed(evt);
            }
        });

        Selectbtn.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Selectbtn.setText("Select");
        Selectbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SelectbtnActionPerformed(evt);
            }
        });

        Timelb.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        Timelb.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Timelb.setText("Preferred time");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(302, 302, 302)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(localb, javax.swing.GroupLayout.DEFAULT_SIZE, 373, Short.MAX_VALUE)
                            .addComponent(Datelb, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Timelb, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(Backbtn)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(245, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 507, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(148, 148, 148))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(Selectbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(357, 357, 357))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(Datelb)
                .addGap(18, 18, 18)
                .addComponent(Timelb)
                .addGap(18, 18, 18)
                .addComponent(localb)
                .addGap(19, 19, 19)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(Selectbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 52, Short.MAX_VALUE)
                .addComponent(Backbtn)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void BackbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BackbtnActionPerformed
        Display.remove(this);
        CardLayout layout = (CardLayout) Display.getLayout();
        layout.previous(Display);
    }//GEN-LAST:event_BackbtnActionPerformed

    private void SelectbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SelectbtnActionPerformed
        int selrow = Fltbl.getSelectedRow();
        if (selrow>=0)
        {
            Flight fl = (Flight) Fltbl.getValueAt(selrow, 0);
            BookSeat panel = new BookSeat(Display,fl,cd);
            Display.add("BookSeat",panel);
            CardLayout layout = (CardLayout) Display.getLayout();
            layout.next(Display);
        }
        else JOptionPane.showMessageDialog(null,"Please select a row!");
    }//GEN-LAST:event_SelectbtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Backbtn;
    private javax.swing.JLabel Datelb;
    private javax.swing.JTable Fltbl;
    private javax.swing.JButton Selectbtn;
    private javax.swing.JLabel Timelb;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel localb;
    // End of variables declaration//GEN-END:variables
}
