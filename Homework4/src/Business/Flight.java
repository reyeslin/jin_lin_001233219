/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Reyes
 */
public class Flight {
    private String id;
    private String fromlocation;
    private String tolocation;
    private int date;
    private String time;
    private int[][] seat;
    private boolean[][] avail;
    private int availnum;

    public Flight(int rownum, int colnum) {
        this.seat = new int[rownum][colnum];
        this.avail = new boolean[rownum][colnum];
    }

    @Override
    public String toString()
    {
        return this.id;
    }
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFromlocation() {
        return fromlocation;
    }

    public void setFromlocation(String fromlocation) {
        this.fromlocation = fromlocation;
    }

    public String getTolocation() {
        return tolocation;
    }

    public void setTolocation(String tolocation) {
        this.tolocation = tolocation;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int[][] getSeat() {
        return seat;
    }

    public void setSeat(int[][] seat) {
        this.seat = seat;
    }

    public boolean[][] getAvail() {
        return avail;
    }

    public void setAvail(int row,int column,boolean bool) {
        this.avail[row][column] = bool;
    }

    public int getAvailnum() {
        return availnum;
    }

    public void setAvailnum(int availnum) {
        this.availnum = availnum;
    }
    
    
    
}
