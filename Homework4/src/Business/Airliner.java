/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import java.util.ArrayList;
/**
 *
 * @author Reyes
 */
public class Airliner {
    private String id;
    private ArrayList<Flight> al;

    public Airliner() {
        this.al = new ArrayList<Flight>();
    }
    
    @Override
    public String toString()
    {
        return this.id;
    }
    
    public Flight searchflight(String st)
    {
        for (Flight fl : al)
            if (fl.getId().equals(st))
                return fl;
        return null;       
    }
    
    public Flight addflight(int rownum, int colnum)
    {
        Flight f = new Flight(rownum,colnum);
        this.al.add(f);
        return f;
    }
    
    public void deleteflight(Flight f)
    {
        this.al.remove(f);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Flight> getAl() {
        return al;
    }

    public void setAl(ArrayList<Flight> al) {
        this.al = al;
    }
    
}
