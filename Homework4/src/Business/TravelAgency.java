/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Reyes
 */
public class TravelAgency {   
    private AirlinerDirectory ad;
    private CustomerDirectory cd;

    public TravelAgency() {
        this.ad = new AirlinerDirectory();
        this.cd = new CustomerDirectory();
    }
    
    public AirlinerDirectory getAd() {
        return ad;
    }

    public void setAd(AirlinerDirectory ad) {
        this.ad = ad;
    }

    public CustomerDirectory getCd() {
        return cd;
    }

    public void setCd(CustomerDirectory cd) {
        this.cd = cd;
    }
    
    
    
}
