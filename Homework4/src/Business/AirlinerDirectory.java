/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import java.util.ArrayList;
/**
 *
 * @author Reyes
 */
public class AirlinerDirectory {
    private ArrayList<Airliner> ad;

    public AirlinerDirectory() {
        this.ad = new ArrayList<Airliner>();
    }

    public Airliner addairliner()
    {
        Airliner al = new Airliner();
        ad.add(al);
        return al;
    }
    
    public void deleteairliner(Airliner al)
    {
        ad.remove(al);
    }
    
    public Airliner searchairliner(String st)
    {
        for (Airliner al:ad)
            if (st.equals(al.getId()))
                return al;
        return null;
    }
    public ArrayList<Airliner> getAd() {
        return ad;
    }

    public void setAd(ArrayList<Airliner> ad) {
        this.ad = ad;
    }
    
}
