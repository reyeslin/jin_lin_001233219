/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.admin;

import javax.swing.JPanel;
import Database.*;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author Reyes
 */
public class ManageUserAccount extends javax.swing.JPanel {
    private JPanel Display;
    private Business b;
    private Admin admin;
    
    public ManageUserAccount(JPanel Display, Business b, Admin admin) {
        initComponents();
        this.Display = Display;
        this.b = b;
        this.admin = admin;
        poptable();
        disabled();
    }
    
    public void disabled()
    {
        Combo1.setEnabled(false);
        Combo2.setEnabled(false);
        Usernametxt.setEnabled(false);
        Passwordtxt.setEnabled(false);
        Createbtn.setEnabled(false);
        Confirmbtn.setEnabled(false);
        Updatebtn.setEnabled(false);
    }
    public void poptable()
    {
        DefaultTableModel dtm = (DefaultTableModel) tbl.getModel();
        dtm.setRowCount(0);
        for (Account a : b.getAc().getAc())
        {
            Object[] row = new Object[4];
            row[0] = a.getUserName();
            row[1] = a;
            if (a.getRole().equals("Admin"))
                row[2] = a.getAdmin().getName();
            if (a.getRole().equals("Supplier"))
                row[2] = a.getSupplier().getName();
            if (a.getRole().equals("Salesperson"))
                row[2] = a.getSalesperson().getFname()+" "+a.getSalesperson().getLname();
            row[3] = a.getRole();
            dtm.addRow(row);
        }       
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl = new javax.swing.JTable();
        Searchtxt = new javax.swing.JTextField();
        Searchbtn = new javax.swing.JButton();
        ViewBtn = new javax.swing.JButton();
        Deletebtn = new javax.swing.JButton();
        CreateUserAccountbtn = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        Usernametxt = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        Passwordtxt = new javax.swing.JTextField();
        Combo1 = new javax.swing.JComboBox<>();
        Combo2 = new javax.swing.JComboBox<>();
        Confirmbtn = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        Createbtn = new javax.swing.JButton();
        Updatebtn = new javax.swing.JButton();
        Backbtn = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Tsukushi A Round Gothic", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 153, 204));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Manage User Account");

        tbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "User Name", "User ID ", "Full Name", "Role"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tbl);

        Searchtxt.setFont(new java.awt.Font("Tsukushi A Round Gothic", 1, 18)); // NOI18N

        Searchbtn.setFont(new java.awt.Font("Tsukushi A Round Gothic", 1, 18)); // NOI18N
        Searchbtn.setText("Search by user ID");
        Searchbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SearchbtnActionPerformed(evt);
            }
        });

        ViewBtn.setFont(new java.awt.Font("Tsukushi A Round Gothic", 1, 18)); // NOI18N
        ViewBtn.setText("View");
        ViewBtn.setPreferredSize(new java.awt.Dimension(84, 34));
        ViewBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ViewBtnActionPerformed(evt);
            }
        });

        Deletebtn.setFont(new java.awt.Font("Tsukushi A Round Gothic", 1, 18)); // NOI18N
        Deletebtn.setText("Delete");
        Deletebtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DeletebtnActionPerformed(evt);
            }
        });

        CreateUserAccountbtn.setFont(new java.awt.Font("Tsukushi A Round Gothic", 1, 18)); // NOI18N
        CreateUserAccountbtn.setText("Create User Account");
        CreateUserAccountbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CreateUserAccountbtnActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tsukushi A Round Gothic", 1, 18)); // NOI18N
        jLabel2.setText("User Name:");

        Usernametxt.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Tsukushi A Round Gothic", 1, 18)); // NOI18N
        jLabel3.setText("Password:");

        Passwordtxt.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        Combo1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Combo1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        Combo2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Combo2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        Confirmbtn.setFont(new java.awt.Font("Tsukushi A Round Gothic", 1, 18)); // NOI18N
        Confirmbtn.setText("Confirm");
        Confirmbtn.setPreferredSize(new java.awt.Dimension(113, 34));
        Confirmbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ConfirmbtnActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tsukushi A Round Gothic", 1, 18)); // NOI18N
        jLabel4.setText("Role:");

        jLabel5.setFont(new java.awt.Font("Tsukushi A Round Gothic", 1, 18)); // NOI18N
        jLabel5.setText("Person:");

        Createbtn.setFont(new java.awt.Font("Tsukushi A Round Gothic", 1, 18)); // NOI18N
        Createbtn.setText("Create");
        Createbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CreatebtnActionPerformed(evt);
            }
        });

        Updatebtn.setFont(new java.awt.Font("Tsukushi A Round Gothic", 1, 18)); // NOI18N
        Updatebtn.setText("Update");
        Updatebtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UpdatebtnActionPerformed(evt);
            }
        });

        Backbtn.setFont(new java.awt.Font("Tsukushi A Round Gothic", 1, 18)); // NOI18N
        Backbtn.setText("<<Back");
        Backbtn.setPreferredSize(new java.awt.Dimension(104, 34));
        Backbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BackbtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(ViewBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Deletebtn))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(Searchtxt, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(Searchbtn))
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(Backbtn, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(CreateUserAccountbtn)
                        .addGap(97, 97, 97))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(66, 66, 66)
                                        .addComponent(Createbtn))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(66, 66, 66)
                                                .addComponent(jLabel3)
                                                .addGap(15, 15, 15))
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel2)
                                                .addGap(18, 18, 18)))
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(Usernametxt, javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(Passwordtxt, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE))))
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(13, 13, 13)
                                    .addComponent(Updatebtn)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(48, 48, 48)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel4))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(Combo2, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(Combo1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(26, 26, 26)
                                .addComponent(Confirmbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(47, Short.MAX_VALUE))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 441, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(201, 201, 201))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jLabel1)
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Searchtxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Searchbtn))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 333, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ViewBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Deletebtn))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(Backbtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(67, 67, 67))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(CreateUserAccountbtn)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Confirmbtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel5)
                                    .addComponent(Combo2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(Combo1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel4)))
                        .addGap(38, 38, 38)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(Usernametxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(Passwordtxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(29, 29, 29)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Createbtn)
                            .addComponent(Updatebtn))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void ConfirmbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ConfirmbtnActionPerformed
        String st = (String) Combo1.getSelectedItem();
        Combo2.removeAllItems();
        if (st.equals("Admin"))
        {
            for (Admin admin : b.getAdminCatalog().getAdminCatalog())
                if (admin.getAccount() == null)
                    Combo2.addItem(String.valueOf(admin));
        }
        
        if (st.equals("Supplier"))
        {
            for (Supplier s : b.getSd().getSd())
                if (s.getAccount() == null)
                    Combo2.addItem(String.valueOf(s));
        }
        
        if (st.equals("Salesperson"))
        {
            for (Salesperson sp : b.getEc().getEc())
                if (sp.getAccount() == null)
                    Combo2.addItem(String.valueOf(sp));
        }
    }//GEN-LAST:event_ConfirmbtnActionPerformed

    private void CreateUserAccountbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CreateUserAccountbtnActionPerformed
        Combo1.setEnabled(true);
        Combo2.setEnabled(true);
        Usernametxt.setEnabled(true);
        Passwordtxt.setEnabled(true);
        Createbtn.setEnabled(true);
        Confirmbtn.setEnabled(true);
        Updatebtn.setEnabled(false);
        Combo1.removeAllItems();
        Combo1.addItem("Admin");
        Combo1.addItem("Supplier");
        Combo1.addItem("Salesperson");
        Combo2.removeAllItems();
        Usernametxt.setText("");
        Passwordtxt.setText("");        
    }//GEN-LAST:event_CreateUserAccountbtnActionPerformed

    private void ViewBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ViewBtnActionPerformed
        int selrow = tbl.getSelectedRow();
        if (selrow < 0)
        {
            JOptionPane.showMessageDialog(null, "Please select a row!");
            return;
        }
        Combo1.setEnabled(false);
        Combo2.setEnabled(false);
        Usernametxt.setEnabled(true);
        Passwordtxt.setEnabled(true);
        Createbtn.setEnabled(false);
        Confirmbtn.setEnabled(false);
        Updatebtn.setEnabled(true);
        Account account = (Account) tbl.getValueAt(selrow, 1);
        String role = account.getRole();                      
        Combo1.removeAllItems();
        Combo2.removeAllItems();
        Combo1.addItem(role);
        Combo2.addItem((String) tbl.getValueAt(selrow, 2));
        Usernametxt.setText(account.getUserName());
        Passwordtxt.setText(account.getPassword());
    }//GEN-LAST:event_ViewBtnActionPerformed

    private void SearchbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SearchbtnActionPerformed
        if (Searchtxt.getText().equals(""))
        {
            poptable();
            return;
        }
        DefaultTableModel dtm = (DefaultTableModel) tbl.getModel();
        dtm.setRowCount(0);
        int searchid = Integer.valueOf(Searchtxt.getText());
        for (Account a : b.getAc().getAc())
            if (a.getAccountid() == searchid)
            {
            Object[] row = new Object[4];
            row[0] = a.getUserName();
            row[1] = a;
            if (a.getRole().equals("Admin"))
                row[2] = a.getAdmin().getName();
            if (a.getRole().equals("Supplier"))
                row[2] = a.getSupplier().getName();
            if (a.getRole().equals("Salesperson"))
                row[2] = a.getSalesperson().getFname()+" "+a.getSalesperson().getLname();
            row[3] = a.getRole();
            dtm.addRow(row);
            return;
            }
        JOptionPane.showMessageDialog(null, "Account ID not found!");
        
    }//GEN-LAST:event_SearchbtnActionPerformed

    private void BackbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BackbtnActionPerformed
        Display.remove(this);
        CardLayout layout = (CardLayout) Display.getLayout();
        layout.previous(Display);        
    }//GEN-LAST:event_BackbtnActionPerformed

    private void CreatebtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CreatebtnActionPerformed
        if ((Usernametxt.equals("")) || (Passwordtxt.equals("")))
        {
            JOptionPane.showMessageDialog(null, "Please enter username or password!");
            return;
        }
        if (Combo2.getItemCount()<=0)
        {
            JOptionPane.showMessageDialog(null, "Please select person!");
            return;
        }
        if (!b.getAc().isValidUsername(Usernametxt.getText(), null))
        {
            JOptionPane.showMessageDialog(null, "Invalid Username!");
            return;            
        }
        String role = (String) Combo1.getSelectedItem();
        String name = (String) Combo2.getSelectedItem();
        if (role.equals("Admin"))
        {
            
            Admin admin = b.getAdminCatalog().findAdmin(name);
            Account account = b.getAc().addAccount();
            account.setUserName(Usernametxt.getText());
            account.setPassword(Passwordtxt.getText());
            account.setRole(role);
            account.setRoleid(admin.getId());
            account.setAdmin(admin);
            admin.setAccount(account);
            JOptionPane.showMessageDialog(null, "Admin created successfully!");
           
        }
        else if (role.equals("Supplier"))
        {            
            Supplier supplier = b.getSd().findSupplier(name);
            Account account = b.getAc().addAccount();
            account.setUserName(Usernametxt.getText());
            account.setPassword(Passwordtxt.getText());
            account.setRole(role);
            account.setRoleid(supplier.getId());
            account.setSupplier(supplier);
            supplier.setAccount(account);
            JOptionPane.showMessageDialog(null, "Supplier created successfully!");
            
        }        
        else if (role.equals("Salesperson"))
        {
            Salesperson salesperson = b.getEc().findSalesperson(name);
            Account account = b.getAc().addAccount();
            account.setUserName(Usernametxt.getText());
            account.setPassword(Passwordtxt.getText());
            account.setRole(role);
            account.setRoleid(salesperson.getId());
            account.setSalesperson(salesperson);
            salesperson.setAccount(account);
            JOptionPane.showMessageDialog(null, "Salesperson created successfully!");
            ;
        }
        else JOptionPane.showMessageDialog(null, "Create failed!");
        disabled();
        poptable();
           
    }//GEN-LAST:event_CreatebtnActionPerformed

    private void UpdatebtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UpdatebtnActionPerformed
        if ((Usernametxt.equals("")) || (Passwordtxt.equals("")))
        {
            JOptionPane.showMessageDialog(null, "Please enter username or password!");
            return;
        }
        Account a = (Account) tbl.getValueAt(tbl.getSelectedRow(), 1);
        if (!b.getAc().isValidUsername(Usernametxt.getText(), a))
        {
            JOptionPane.showMessageDialog(null, "Invalid Username!");
            return;            
        }
        
       a.setUserName(Usernametxt.getText());
       a.setPassword(Passwordtxt.getText());
       disabled();
       poptable();
       JOptionPane.showMessageDialog(null, "Updated Successfully!");
    }//GEN-LAST:event_UpdatebtnActionPerformed

    private void DeletebtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DeletebtnActionPerformed
        int selrow = tbl.getSelectedRow();
        if (selrow < 0)
        {
            JOptionPane.showMessageDialog(null, "Please select a row!");
            return;
        }
        if (JOptionPane.showConfirmDialog(null, "Do you want to delete this row?","Warning",JOptionPane.YES_NO_OPTION)== JOptionPane.NO_OPTION)
            return;
        
        Account a = (Account)tbl.getValueAt(tbl.getSelectedRow(), 1);
        String role = a.getRole();
        if (role.equals("Admin"))
        {
            if (a.getAdmin() == admin)
            {
                JOptionPane.showMessageDialog(null, "You cannot delete yourselve!");
                return;
            }
            Admin am = a.getAdmin();
            am.setAccount(null);
            b.getAc().deleteAccount(a);
        }
        if (role.equals("Supplier"))
        {
            Supplier s = a.getSupplier();
            s.setAccount(null);
            b.getAc().deleteAccount(a);
        }
        if (role.equals("Salesperson"))
        {
            Salesperson sp = a.getSalesperson();
            sp.setAccount(null);
            b.getAc().deleteAccount(a);
        }
        poptable();
        JOptionPane.showMessageDialog(null, "Deleted Successfully!");
            
    }//GEN-LAST:event_DeletebtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Backbtn;
    private javax.swing.JComboBox<String> Combo1;
    private javax.swing.JComboBox<String> Combo2;
    private javax.swing.JButton Confirmbtn;
    private javax.swing.JButton CreateUserAccountbtn;
    private javax.swing.JButton Createbtn;
    private javax.swing.JButton Deletebtn;
    private javax.swing.JTextField Passwordtxt;
    private javax.swing.JButton Searchbtn;
    private javax.swing.JTextField Searchtxt;
    private javax.swing.JButton Updatebtn;
    private javax.swing.JTextField Usernametxt;
    private javax.swing.JButton ViewBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tbl;
    // End of variables declaration//GEN-END:variables
}
