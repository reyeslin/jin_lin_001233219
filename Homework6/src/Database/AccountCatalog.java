/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.util.ArrayList;

/**
 *
 * @author win10
 */
public class AccountCatalog {
    
    private ArrayList<Account> ac;

    public AccountCatalog() {
        this.ac = new ArrayList<Account>();
    }

    public ArrayList<Account> getAc() {
        return ac;
    }

    public void setAc(ArrayList<Account> ac) {
        this.ac = ac;
    }
    
    public Account addAccount(){
        Account a  = new Account();
        ac.add(a);
        return a;
    }
    
    public void deleteAccount(Account a){
        ac.remove(a);
    }
    
    
    public Account isValidAccount(String userid, String password)
    {
        for (Account a : ac)
            if ((userid.equals(a.getUserName())) && (password.equals(a.getPassword())))
                return a;
        return null;
    }   
    
    public boolean isValidUsername(String username,Account account)
    {
        for (Account a : ac)
            if ((a!=account) && (a.getUserName().equals(username)))
                return false;
        return true;
    }
    
}
