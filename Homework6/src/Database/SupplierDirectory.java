/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.util.ArrayList;

/**
 *
 * @author Reyes
 */
public class SupplierDirectory {
    private ArrayList<Supplier> sd;

    public SupplierDirectory() {
        sd = new ArrayList<Supplier>();
    }

    public ArrayList<Supplier> getSd() {
        return sd;
    }

    public void setSd(ArrayList<Supplier> sd) {
        this.sd = sd;
        
    }
    
    public Supplier addSupplier()
    {
        Supplier s = new Supplier();
        sd.add(s);
        return s;
    }
    
    public void removeSupplier(Supplier s)
    {
        sd.remove(s);
    }
    
    public Supplier findSupplier(String name)
    {
        for (Supplier s : sd)
            if (s.getName().equals(name))
                return s;
        return null;
    }
    
       public Supplier findSupplier(int id)
    {
        for (Supplier s : sd)
            if (s.getId() == id)
                return s;
        return null;
    }
    
    
    
}
