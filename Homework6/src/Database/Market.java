/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.util.ArrayList;

/**
 *
 * @author Reyes
 */
public class Market {
    private String name;
    private double total;
    private ArrayList<Customer> customerList;
    private MarketOfferCatalog marketOfferCatalog;

    public Market() {
        this.customerList = new ArrayList<Customer>();
        marketOfferCatalog = new MarketOfferCatalog();
    }

    public MarketOfferCatalog getMarketOfferCatalog() {
        return marketOfferCatalog;
    }

    public void setMarketOfferCatalog(MarketOfferCatalog marketOfferCatalog) {
        this.marketOfferCatalog = marketOfferCatalog;
    }

    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public ArrayList<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(ArrayList<Customer> customerList) {
        this.customerList = customerList;
    }
    
    public Customer addCustomer()
    {
        Customer c = new Customer();
        customerList.add(c);
        return c;
    }
    
    public void deleteCustomer(Customer c)
    {
        customerList.remove(c);
    }
    

    
    @Override
    public String toString(){
        return this.name;
    }
    
}
