/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;
import java.util.ArrayList;
/**
 *
 * @author Reyes
 */
public class MarketList {
    private ArrayList<Market> ml;

    public MarketList() {
        ml = new ArrayList<Market>();
    }

    public ArrayList<Market> getMl() {
        return ml;
    }
    
    public Market addMarket()
    {
        Market mk = new Market();
        ml.add(mk);
        return mk;
    }

    public void deleteMarket(Market mk)
    {
        ml.remove(mk);
    }
    
    public Customer findCustomerByName(String s){
        for(Market market:ml){
            for(Customer c:market.getCustomerList()){
                if(c.getCustomerName().equals(s)){
                    return c;
                }
            }
        }
        
        return null;
    }
    
}
