/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.util.ArrayList;

/**
 *
 * @author Reyes
 */
public class Order {
    private int orderid;
    private static int ordercount = 10000;
    private ArrayList<OrderItem> order;
    private Customer c;
    private Salesperson sp;
    private double total = 0;

    public Order() {
        order = new ArrayList<OrderItem>();
        ordercount++;
        orderid = ordercount;
    }

    public int getOrderid() {
        return orderid;
    }

    public ArrayList<OrderItem> getOrder() {
        return order;
    }

    public OrderItem addOrderItem()
    {
        OrderItem oi = new OrderItem();
        order.add(oi);
        return oi;
    }
    
    public void deleteOrderItem(OrderItem oi)
    {
        order.remove(oi);
    }

    public Customer getC() {
        return c;
    }

    public void setC(Customer c) {
        this.c = c;
    }   

    public Salesperson getSp() {
        return sp;
    }

    public void setSp(Salesperson sp) {
        this.sp = sp;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    

    public void calculateTotal() {
        total = 0;
        for(OrderItem item:order){
            total = total + (item.getActualprice()-item.getTarget())*item.getQuantity();
        }
    }
    
   public double totalPrice(){
        double totalPrice = 0;
        for(OrderItem item:order){
            totalPrice = totalPrice + item.getActualprice();
        }
        return totalPrice;
    }
    
    @Override
    public String toString(){
        return String.valueOf(this.getOrderid());
    }
}
 