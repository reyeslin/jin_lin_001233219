/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

/**
 *
 * @author win10
 */
public class Account {
    
    private String userName;
    private String password;
    private String role;
    private int roleid;
    private int accountid;
    private static int accountcount = 100;
    private Admin admin;
    private Supplier supplier;
    private Salesperson salesperson;

    public Account() {
        accountcount++;
        accountid = accountcount;
    }

    @Override
    public String toString()
    {
        return String.valueOf(accountid);
    }

    public Admin getAdmin() {
        return admin;
    }

    public static int getAccountcount() {
        return accountcount;
    }
    

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public Salesperson getSalesperson() {
        return salesperson;
    }

    public void setSalesperson(Salesperson salesperson) {
        this.salesperson = salesperson;
    }
    
    
    public int getAccountid() {
        return accountid;
    }

    public int getRoleid() {
        return roleid;
    }

    public void setRoleid(int roleid) {
        this.roleid = roleid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    
    
    
    
}
