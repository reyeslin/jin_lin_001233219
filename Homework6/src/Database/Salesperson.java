/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.util.ArrayList;

/**
 *
 * @author Reyes
 */
public class Salesperson {
    private String lname;
    private String fname;
    private double revenue;
    private OrderCatalog oc;
    private int id;
    private static int salespersoncount = 1000;
    private Account account;

    public Salesperson() {
        oc = new OrderCatalog();
        account = null;
        salespersoncount++;
        id = salespersoncount;
    }

    @Override
    public String toString()
    {
        return fname+" "+lname;
    }
    public int getId() {
        return id;
    }

    public static int getSalespersoncount() {
        return salespersoncount;
    }
    

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
    
    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public double getRevenue() {
        return revenue;
    }

    public void setRevenue(double revenue) {
        this.revenue = revenue;
    }

    public OrderCatalog getOc() {
        return oc;
    }

    public void setOc(OrderCatalog oc) {
        this.oc = oc;
    }
    
    public void calculateRevenue(){
        revenue = 0;
        for(Order order:oc.getOc()){
            revenue = revenue + order.getTotal();
        }
    }
    
    
}
