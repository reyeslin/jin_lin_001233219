/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;
import java.io.*;
import java.util.Random;
/**
 *
 * @author Reyes
 */
public class ConfigureABusiness {
    public static Business Initialization(String name)
    {
        Business b = new Business(name);
        //System Admin
        Admin admin = b.getAdminCatalog().addAdmin();
        admin.setName("Reyes Lin");
        Account account1 = b.getAc().addAccount();
        account1.setRole("Admin");
        account1.setUserName("admin");
        account1.setPassword("123");
        account1.setRoleid(admin.getId());
        account1.setAdmin(admin);
        admin.setAccount(account1);
        
        try {
            File file = new File("hw6.txt");
            BufferedReader br = new BufferedReader(new FileReader(file));
            String read;
            read = br.readLine();
            int prototalnum = 0;
            
            //Supplier & Market Offer
            int suppliernum = Integer.parseInt(read);            
            for (int i = 0; i < suppliernum; i++)
            {
                read = br.readLine();
                String[] st = read.split(",");
                Supplier supplier = b.getSd().addSupplier();
                supplier.setName(st[0]);
                Account account = b.getAc().addAccount();
                account.setUserName(st[0]);
                account.setPassword("123");
                account.setRole("Supplier");
                account.setRoleid(supplier.getId());
                account.setSupplier(supplier);
                supplier.setAccount(account);                
                int productnum = Integer.parseInt(st[1]);
                prototalnum += productnum;
                for (int j = 0; j < productnum; j++)
                {
                    Product product = supplier.getPc().addProduct();
                    read = br.readLine();
                    String[] pro = read.split(",");
                    product.setName(pro[0]);
                    product.setCost(Double.parseDouble(pro[1]));
                    MarketOffer marketOffer = b.getMoc().addMarketOffer();
                    marketOffer.setProduct(product);
                    marketOffer.setFloor(Double.parseDouble(pro[2]));
                    marketOffer.setCeiling(Double.parseDouble(pro[3]));
                    product.setMo(marketOffer);
                }                
            }
            
            //Market & Customer & Market Offer
            read = br.readLine();
            int marketnum = Integer.parseInt(read);
            int cusnum = 0;
            for (int i = 0; i < marketnum; i++)
            {
                read = br.readLine();
                String[] st = read.split(",");
                Market market = b.getMl().addMarket();
                market.setName(st[0]);
                int customernum = Integer.parseInt(st[1]);
                cusnum += customernum;
                String[] cus = br.readLine().split(",");
                for (int j = 0; j < customernum; j++)
                {
                    Customer customer = market.addCustomer();
                    customer.setMarket(market);
                    customer.setCustomerName(cus[j]);
                }
                
                for (int j = 0; j < prototalnum; j++)
                {
                    String[] mo = br.readLine().split(",");
                    MarketOffer marketOffer = market.getMarketOfferCatalog().addMarketOffer();
                    marketOffer.setProduct(b.getMoc().getMarketOffer(j).getProduct());
                    marketOffer.setFloor(Double.parseDouble(mo[0]));
                    marketOffer.setCeiling(Double.parseDouble(mo[1]));
                    marketOffer.setTarget(Double.parseDouble(mo[2]));                    
                }                
            }
        String[] fname = {"Alice","Bob","Charlie","David","Eve","Frank"};
        String[] lname = {"Justin","Ivan","Oscar","Veronica","Reyes","Wendy"};
        for (int i = 0; i < 6; i++)
            for (int j = 0; j < 6; j++)
            {
                Salesperson salesperson = b.getEc().addSalesperson();
                salesperson.setFname(fname[i]);
                salesperson.setLname(lname[j]);
                Account account = b.getAc().addAccount();
                account.setRole("Salesperson");
                String s = lname[j]+fname[i];
                account.setUserName(s.toLowerCase());
                account.setPassword("123");
                account.setRoleid(salesperson.getId());
                account.setSalesperson(salesperson);
                salesperson.setAccount(account);
            }
        
        int ordernum = 120;
        Random r = new Random();
        for (int i = 0; i< ordernum; i++)
        {                
            Salesperson salesperson = b.getEc().getEc().get(r.nextInt(36));
            Market market = b.getMl().getMl().get(r.nextInt(marketnum)); 
            Customer customer = market.getCustomerList().get(r.nextInt(market.getCustomerList().size()));
            
            Order order = customer.getOc().addOrder();
            order.setC(customer);
            order.setSp(salesperson);
            salesperson.getOc().addexistorder(order);
                  
            int orderitemnum=0;
            //if (r.nextDouble()>0.4) orderitemnum = r.nextInt(40)+1;
            //else 
                orderitemnum = r.nextInt(10)+1;
            for (int j = 0; j < orderitemnum; j++)
            {
                int proid = r.nextInt(prototalnum);
                int quantity=0;
                if (r.nextDouble()>0.95) quantity = r.nextInt(1000)+1;
                else quantity= r.nextInt(100)+1;
                MarketOffer marketOffer = market.getMarketOfferCatalog().getMoc().get(proid);
                OrderItem orderItem = order.addOrderItem();
                orderItem.setQuantity(quantity);
                orderItem.setProduct(marketOffer.getProduct());
                orderItem.setTarget(marketOffer.getTarget());
                double price = r.nextDouble();
                if (r.nextDouble()>0.3)                
                     orderItem.setActualprice(price*(marketOffer.getCeiling()-marketOffer.getTarget())+marketOffer.getTarget());
                else orderItem.setActualprice(price*(marketOffer.getTarget()-marketOffer.getFloor())+marketOffer.getFloor());
            }
            order.calculateTotal(); 
        }
        
        for (Salesperson sp : b.getEc().getEc())
            sp.calculateRevenue();

        
            
            
            br.close();
        } catch (FileNotFoundException ex) {  
            ex.printStackTrace();  
        } catch (IOException ex) {  
            ex.printStackTrace();  
        }  
        
        

        return b;
    }
    
}
