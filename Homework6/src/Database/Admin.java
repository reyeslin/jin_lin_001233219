/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.util.ArrayList;

/**
 *
 * @author win10
 */
public class Admin {
    
    private String name;
    private int id;
    private static int admincount = 0;
    private Account account;

    public Admin() {
        account = null;
        admincount++;
        id = admincount;
    }

    @Override 
    public String toString()
    {
        return name;
    }
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    
    
    
    
    
        
    
}
