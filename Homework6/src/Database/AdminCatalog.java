/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.util.ArrayList;

/**
 *
 * @author win10
 */
public class AdminCatalog {
    
    private ArrayList<Admin> adminCatalog;

    public AdminCatalog() {
        this.adminCatalog = new ArrayList<Admin>();
    }

    public ArrayList<Admin> getAdminCatalog() {
        return adminCatalog;
    }

    public void setAdminCatalog(ArrayList<Admin> adminCatalog) {
        this.adminCatalog = adminCatalog;
    }
    
    public Admin addAdmin(){
        Admin a = new Admin();
        adminCatalog.add(a);
        return a;
    }
    
    public Admin findAdmin(String name)
    {
        for (Admin admin : adminCatalog)
            if (admin.getName().equals(name))
                return admin;
        return null;
    }
}
