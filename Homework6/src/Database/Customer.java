/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

/**
 *
 * @author win10
 */
public class Customer {
    
    private String customerName;
    private OrderCatalog oc;
    private Market market;

    public Customer() {
        oc = new OrderCatalog();
    }    

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }   

    public OrderCatalog getOc() {
        return oc;
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }
    
    
    
    
}
