/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

/**
 *
 * @author Reyes
 */
public class Business {
    private String name;
    private MarketList ml;
    private EmployeeCatalog ec;
    private SupplierDirectory sd;
    private MarketOfferCatalog moc;
    private AccountCatalog ac;
    private AdminCatalog adminCatalog;

    public Business(String name) {
        this.ac = new AccountCatalog();
        this.ec = new EmployeeCatalog();
        this.ml = new MarketList();
        this.moc = new MarketOfferCatalog();
        this.sd = new SupplierDirectory();
        this.adminCatalog = new AdminCatalog();
        this.name = name;
    }

    public AdminCatalog getAdminCatalog() {
        return adminCatalog;
    }

    public void setAdminCatalog(AdminCatalog adminCatalog) {
        this.adminCatalog = adminCatalog;
    }

    public MarketList getMl() {
        return ml;
    }

    public void setMl(MarketList ml) {
        this.ml = ml;
    }

    public EmployeeCatalog getEc() {
        return ec;
    }

    public void setEc(EmployeeCatalog ec) {
        this.ec = ec;
    }

    public SupplierDirectory getSd() {
        return sd;
    }

    public void setSd(SupplierDirectory sd) {
        this.sd = sd;
    }

    public MarketOfferCatalog getMoc() {
        return moc;
    }

    public void setMoc(MarketOfferCatalog moc) {
        this.moc = moc;
    }

    public AccountCatalog getAc() {
        return ac;
    }

    public void setAc(AccountCatalog ac) {
        this.ac = ac;
    }
    
    
}
