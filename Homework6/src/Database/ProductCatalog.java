/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.util.ArrayList;

/**
 *
 * @author Reyes
 */
public class ProductCatalog {
    private ArrayList<Product> pc;

    public ProductCatalog() {
        pc = new ArrayList<Product>();
    }

    public ArrayList<Product> getPc() {
        return pc;
    }
    
    public Product addProduct()
    {
        Product p = new Product();
        pc.add(p);
        return p;
    }
    
    public void deleteProduct(Product p)
    {
        pc.remove(p);
    }

    public Product searchProduct(int productID) {
        //To change body of generated methods, choose Tools | Templates.
//        Product product = new Product();
        for(Product p: pc )
        {
            if(p.getId() == productID)
            {
                return p;
            }
        }
        return null;
    }
    
    
}
