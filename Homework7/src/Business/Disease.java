/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author james
 */
public class Disease {

    private String Name;
    private static int counter = 1;
    private int DiseaseID;

    public Disease() {
        DiseaseID = counter;
        counter++;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getDiseaseID() {
        return DiseaseID;
    }

    public void setDiseaseID(int DiseaseID) {
        this.DiseaseID = DiseaseID;
    }

    @Override
    public String toString()
    {
        return Name;
    }
}
