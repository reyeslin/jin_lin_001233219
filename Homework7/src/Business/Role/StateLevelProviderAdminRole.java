/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import userinterface.State_LevelAdministrativeRole.StateLevelProviderAdminWorkArea;
import javax.swing.JPanel;

/**
 *
 * @author Reyes
 */
public class StateLevelProviderAdminRole extends Role {

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        return new StateLevelProviderAdminWorkArea(userProcessContainer, enterprise, business);
    }

}
