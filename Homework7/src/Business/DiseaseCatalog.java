/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Network.Network;
import java.util.ArrayList;

/**
 *
 * @author james
 */
public class DiseaseCatalog {

    private ArrayList<Disease> diseaseList;

    public DiseaseCatalog() {
        diseaseList = new ArrayList<>();
    }

    public ArrayList<Disease> getDiseaseList() {
        return diseaseList;
    }

    public void setDiseaseList(ArrayList<Disease> diseaseList) {
        this.diseaseList = diseaseList;
    }

    public Disease createAndAdddisease() {
        Disease disease = new Disease();
        diseaseList.add(disease);
        return disease;
    }

    public void removeDisease(Disease disease) {
        diseaseList.remove(disease);
    }

}
