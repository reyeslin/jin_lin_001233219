/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Role.Role;
import Business.Role.StateLevelProviderAdminRole;
import java.util.ArrayList;

/**
 *
 * @author Reyes
 */
public class StateLevelProviderEnterprise extends Enterprise{
    
     public StateLevelProviderEnterprise(String name) {
        super(name, Enterprise.EnterpriseType.StateLevelProvider);
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new StateLevelProviderAdminRole());
        return roles;
        
    }
}
