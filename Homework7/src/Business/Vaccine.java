/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import org.w3c.dom.NameList;

/**
 *
 * @author james
 */
public class Vaccine {

    private String Name;
    private static int counter = 1;
    private int VaccineID;
    private int Quantity;
    private Disease disease;

    public Vaccine() {
        VaccineID = counter;
        counter++;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getVaccineID() {
        return VaccineID;
    }

    public void setVaccineID(int VaccineID) {
        this.VaccineID = VaccineID;
    }

    public Disease getDisease() {
        return disease;
    }

    public void setDisease(Disease disease) {
        this.disease = disease;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int Quantity) {
        this.Quantity = Quantity;
    }

    @Override
    public String toString()
    {
        return Name;
    }
}
