/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.util.ArrayList;

/**
 *
 * @author Reyes
 */
public class PersonDirectory {
    private ArrayList<Person> pd;

    public PersonDirectory() {
        pd = new ArrayList<Person>();
    }

    public ArrayList<Person> getPd() {
        return pd; 
    }
    
    public Person AddPerson()
    {
        Person p = new Person();
        pd.add(p);
        return p;
    }
    
    public Person SearchByLastName(String st)
    {
        for (Person p : pd)
            if (st.equals(p.getLastname()))
                return p;
        return null;        
    }
    
    public Person SearchPerson(String ln, String fn, String un)
    {
        for (Person p : pd)
            if ((ln.equals(p.getLastname())) && (fn.equals(p.getFirstname())))
            {
                if ((un.equals("---")) && (p.getUad().size()==0)) return p;
                for (UserAccount ua : p.getUad())
                    if (un.equals(ua.getUserid()))
                        return p;
                
            }
        return null;
    }
    
    public void DeletePerson(Person p)
    {
        pd.remove(p);
    }
}
