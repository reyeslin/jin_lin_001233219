/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

/**
 *
 * @author Reyes
 */
public class ConfigureABusiness {
    public static Business Initialization(String st)
    {
        Business b = new Business(st);
        PersonDirectory pd = b.getPd();
        UserAccountDirectory uad = b.getUad();
        
        Person p1 = pd.AddPerson();
        p1.setFirstname("Jin");
        p1.setLastname("Lin");
        Person p2 = pd.AddPerson();
        p2.setFirstname("Goby");
        p2.setLastname("Pasha");
        
        
      
            UserAccount ua = uad.AddUserAccount();
            ua.setUserid("reyes");
            ua.setPassword("1");
            ua.setPerson(p1);
            ua.setRole("System Admin");
            ua.setStatus(true);
            p1.addUserAccount(ua);
            
            UserAccount ua1 = uad.AddUserAccount();
            ua1.setUserid("a");
            ua1.setPassword("1");
            ua1.setPerson(p2);
            ua1.setRole("HR Admin");
            ua1.setStatus(true);
            p2.addUserAccount(ua1);            
            
       
        
        return b;
    }
}
