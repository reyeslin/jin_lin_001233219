/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.util.ArrayList;

/**
 *
 * @author Reyes
 */
public class Person {
    private String lastname;
    private String firstname;
    private ArrayList<UserAccount> uad;

    public Person() {
        uad = new ArrayList<UserAccount>();
    }    
    

    @Override
    public String toString()
    {
        return firstname+" "+lastname;        
    }

    public ArrayList<UserAccount> getUad() {
        return uad;
    }
    
    public void addUserAccount(UserAccount ua)
    {
        uad.add(ua);
    }
    
    public void deleteUserAccount(UserAccount ua)
    {
        uad.remove(ua);
    }
    
    public UserAccount searchUserAccount(String st)
    {
        for (UserAccount ua : uad)
            if (st.equals(ua.getUserid()))
                return ua;
        return null;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    
    
}
