/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.util.ArrayList;

/**
 *
 * @author Reyes
 */
public class UserAccountDirectory {
    private ArrayList<UserAccount> uad;

    public UserAccountDirectory() {
        uad = new ArrayList<UserAccount>();
    }

    public ArrayList<UserAccount> getUad() {
        return uad;
    }
    
    public UserAccount AddUserAccount()
    {
        UserAccount ua = new UserAccount();
        uad.add(ua);
        return ua;
    }
    
    public UserAccount SearchAccount(String st)
    {
        for (UserAccount ua : uad)
            if (st.equals(ua.getUserid()))
                return ua;
        return null;
    }
    
    public void DeleteAccount(UserAccount ua)
    {
        uad.remove(ua);
    }
    
    public UserAccount isValidUser(String userid, String password)
    {
        for (UserAccount ua : uad)
            if ((userid.equals(ua.getUserid())) && (password.equals(ua.getPassword())))
                return ua;
        return null;
    }   
    
}
