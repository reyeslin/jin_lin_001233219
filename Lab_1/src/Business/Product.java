/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Reyes
 */
public class Product {
    private String Name;
    private String Price;
    private String AvailiblityNumber;
    private String Description;

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String Price) {
        this.Price = Price;
    }

    public String getAvailiblityNumber() {
        return AvailiblityNumber;
    }

    public void setAvailiblityNumber(String AvailiblityNumber) {
        this.AvailiblityNumber = AvailiblityNumber;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }
    
}
