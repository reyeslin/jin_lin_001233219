/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user;

/**
 *
 * @author Reyes
 */
public class FinancialAccounts {
    private String chkacc_date;
    private String savacc_date;
    private Boolean chkacc_act;
    private Boolean savacc_act;
    private String chkacc_debt;
    private String savacc_debt;
    private String chkacc_cred;
    private String savacc_cred;

    public String getChkacc_date() {
        return chkacc_date;
    }

    public void setChkacc_date(String chkacc_date) {
        this.chkacc_date = chkacc_date;
    }

    public String getSavacc_date() {
        return savacc_date;
    }

    public void setSavacc_date(String savacc_date) {
        this.savacc_date = savacc_date;
    }

    public Boolean getChkacc_act() {
        return chkacc_act;
    }

    public void setChkacc_act(Boolean chkacc_act) {
        this.chkacc_act = chkacc_act;
    }

    public Boolean getSavacc_act() {
        return savacc_act;
    }

    public void setSavacc_act(Boolean savacc_act) {
        this.savacc_act = savacc_act;
    }

    public String getChkacc_debt() {
        return chkacc_debt;
    }

    public void setChkacc_debt(String chkacc_debt) {
        this.chkacc_debt = chkacc_debt;
    }

    public String getSavacc_debt() {
        return savacc_debt;
    }

    public void setSavacc_debt(String savacc_debt) {
        this.savacc_debt = savacc_debt;
    }

    public String getChkacc_cred() {
        return chkacc_cred;
    }

    public void setChkacc_cred(String chkacc_cred) {
        this.chkacc_cred = chkacc_cred;
    }

    public String getSavacc_cred() {
        return savacc_cred;
    }

    public void setSavacc_cred(String savacc_cred) {
        this.savacc_cred = savacc_cred;
    }
    
}
