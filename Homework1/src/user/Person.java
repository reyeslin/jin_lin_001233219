/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user;

/**
 *
 * @author LJ
 */
public class Person {
    
    private String imagepath;  
    
    private Address address;
    private LicenseData lidata;
    private CreditCard crcard;
    private FinancialAccounts finacc;
    
    public Person(){
        this.address = new Address();
        this.lidata = new LicenseData();
        this.crcard = new CreditCard();
        this.finacc = new FinancialAccounts(); 
}
    

    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    public String getId() {       
        return lidata.getId();
    }

    public void setId(String id) {
        lidata.setId(id);   
    }

    public String getDob() {
        return lidata.getDob();
    }

    public void setDob(String dob) {
        lidata.setDob(dob);
    }

    public String getIssdate() {
        return lidata.getIssdate();
    }

    public void setIssdate(String issdate) {
        lidata.setIssdate(issdate);
    }

    public String getExpdate() {
        return lidata.getExpdate();
    }

    public void setExpdate(String expdate) {
        lidata.setExpdate(expdate);
    }

    public String getHgt() {
        return lidata.getHgt();
    }

    public void setHgt(String hgt) {
        lidata.setHgt(hgt);
    }

    public String getBank() {
        return crcard.getBank();
    }

    public void setBank(String bank) {
        crcard.setBank(bank);
    }

    public String getNum() {
        return crcard.getNum();
    }

    public void setNum(String num) {
        crcard.setNum(num);
    }

    public String getValiddate() {
        return crcard.getValiddate();
    }

    public void setValiddate(String validdate) {
        crcard.setValiddate(validdate);
    }

    public String getCsc() {
        return crcard.getCsc();
    }

    public void setCsc(String csc) {
        crcard.setCsc(csc);
    }

    public String getColor() {
        return crcard.getColor();
    }

    public void setColor(String color) {
        crcard.setColor(color);
    }

    public String getApt() {
        return address.getApt();
    }

    public void setApt(String Apt) {
        address.setApt(Apt);
    }

    public String getStreet() {
        return address.getStreet();
    }

    public void setStreet(String Street) {
        address.setStreet(Street);
    }

    public String getCity() {
        return address.getCity();
    }

    public void setCity(String city) {
        address.setCity(city);
    }

    public String getState() {
        return address.getState();
    }

    public void setState(String state) {
        address.setState(state);
    }

    public String getZip() {
        return address.getZip();
    }

    public void setZip(String zip) {
        address.setZip(zip);
    }

    public String getChkacc_date() {
        return finacc.getChkacc_date();
    }

    public void setChkacc_date(String chkacc_date) {
        finacc.setChkacc_date(chkacc_date);
    }

    public String getSavacc_date() {
        return finacc.getSavacc_date();
    }

    public void setSavacc_date(String savacc_date) {
        finacc.setSavacc_date(savacc_date);
    }

    public Boolean getChkacc_act() {
        return finacc.getChkacc_act();
    }

    public void setChkacc_act(Boolean chkacc_act) {
        finacc.setChkacc_act(chkacc_act);
    }

    public Boolean getSavacc_act() {
        return finacc.getSavacc_act();
    }

    public void setSavacc_act(Boolean savacc_act) {
        finacc.setSavacc_act(savacc_act);
    }

    public String getChkacc_debt() {
        return finacc.getChkacc_debt();
    }

    public void setChkacc_debt(String chkacc_debt) {
        finacc.setChkacc_debt(chkacc_debt);
    }

    public String getSavacc_debt() {
        return finacc.getSavacc_debt();
    }

    public void setSavacc_debt(String savacc_debt) {
        finacc.setSavacc_debt(savacc_debt);
    }

    public String getChkacc_cred() {
        return finacc.getChkacc_cred();
    }

    public void setChkacc_cred(String chkacc_cred) {
        finacc.setChkacc_cred(chkacc_cred);
    }

    public String getSavacc_cred() {
        return finacc.getSavacc_cred();
    }

    public void setSavacc_cred(String savacc_cred) {
        finacc.setSavacc_cred(savacc_cred);
    }


    

    

    
}
