/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework3;
import java.util.ArrayList;
/**
 *
 * @author Reyes
 */
public class AirlinerManager {
    private ArrayList<Airliners> am;

    public AirlinerManager() {
        am = new ArrayList<Airliners>();
    }
    

    public ArrayList<Airliners> getAm() {
        return am;
    }

    public void setAm(ArrayList<Airliners> al) {
        this.am = al;
    }
    
    public Airliners add()
    {
        Airliners al = new Airliners();
        am.add(al);
        return al;
    }
}
