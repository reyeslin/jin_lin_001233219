/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework3;
import java.util.ArrayList;
/**
 *
 * @author Reyes
 */
public class Airliners {
    private String airlinerid;
    private ArrayList<Airplanes> fleet;
    private ArrayList<Flights> flight;
    private int fleettot;
    
    public Airliners()
     {
         fleet = new ArrayList<Airplanes>();
         flight = new ArrayList<Flights>();  
     }

    public String getAirlinerid() {
        return airlinerid;
    }

    public void setAirlinerid(String airlinerid) {
        this.airlinerid = airlinerid;
    }    

    public ArrayList<Airplanes> getFleet() {
        return fleet;
    }

    public void setFleet(ArrayList<Airplanes> fleet) {
        this.fleet = fleet;
    }

    public ArrayList<Flights> getFlight() {
        return flight;
    }

    public void setFlight(ArrayList<Flights> flight) {
        this.flight = flight;
    }

    public int getFleettot() {
        return fleettot;    
    }

    public void setFleettot(int fleettot) {
        this.fleettot = fleettot;
    }
    
    public Airplanes addAirplane()
    {
        Airplanes ap = new Airplanes();
        fleet.add(ap);
        return ap;
    }
    
    public Flights addFlight()
    {
        Flights fl = new Flights();
        flight.add(fl);
        return fl;        
    }
    
    
}
