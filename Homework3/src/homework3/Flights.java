/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework3;

/**
 *
 * @author Reyes
 */
public class Flights {
    private String id;
    private int tot;
    private int[][] price = new int[25][6];
    private int[][] seatsavail = new int[25][6];

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int[][] getPrice() {
        return price;
    }

    public void setPrice(int[][] price) {
        this.price = price;
    }

    public int[][] getSeatsavail() {
        return seatsavail;
    }

    public void setSeatsavail(int row, int column, int num) {        
        this.seatsavail[row][column] = num;
    }

    public int getTot() {
        return tot;
    }

    public void setTot(int tot) {
        this.tot = tot;
    }
    
}
