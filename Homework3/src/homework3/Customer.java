/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework3;

/**
 *
 * @author Reyes
 */
public class Customer {
    private String lname;
    private String fname;
    private String selflight;
    private int seatrow;
    private int seatcolumn;

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getSelflight() {
        return selflight;
    }

    public void setSelflight(String selflight) {
        this.selflight = selflight;
    } 

    public int getSeatrow() {
        return seatrow;
    }

    public void setSeatrow(int seatrow) {
        this.seatrow = seatrow;
    }

    public int getSeatcolumn() {
        return seatcolumn;
    }

    public void setSeatcolumn(int seatcolumn) {
        this.seatcolumn = seatcolumn;
    }
    
}
