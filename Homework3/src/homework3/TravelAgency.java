/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework3;
import java.io.*;
import java.util.ArrayList;
/**
 *
 * @author Reyes
 */
public class TravelAgency {
    private AirlinerManager am = new AirlinerManager();
    private CustomerCatalog cc = new CustomerCatalog();
    private MasterSchedule ms;
    private Customer cus;
    private int tot;
    //private int[][] seats = new int[25][6];  
    //private int[][] seatsavail = new int[25][6];
    
   
    public void readfile()
    {
        int i,j,k,l;  
        Airliners al;
        Airplanes ap;
        Flights fl;
        int flnum,alnum,apnum,cusnum;
        
        try {  
           
            File file = new File("hw3.csv");
            BufferedReader bufread;  
            String read;  
            bufread = new BufferedReader(new FileReader(file));  
            alnum = Integer.valueOf(bufread.readLine());
            for (i = 0; i < alnum; i++)
            {                
                al = am.add();                
                read = bufread.readLine();
                al.setAirlinerid(read);   
                
                apnum = Integer.valueOf(bufread.readLine());
                for (j = 0; j < apnum; j++)
                {
                    read = bufread.readLine();                   
                    ap = al.addAirplane();
                    ap.setId(read);
                    
                    int[][] seats = new int[25][6];
                    for (k = 0; k < 25; k++)
                    {
                        read = bufread.readLine();
                        String[] row = read.split(",");
                        for (l = 0; l < 6; l++)
                            seats[k][l] = Integer.valueOf(row[l]);
                    }
                    ap.setSeats(seats);  
                    
                    flnum = Integer.valueOf(bufread.readLine());
                    for (k = 0; k < flnum; k++)
                    {
                        fl = al.addFlight();
                        read = bufread.readLine();
                        fl.setId(read);
                        fl.setPrice(seats);
                    }
                }
            }           
            cusnum = Integer.valueOf(bufread.readLine());
            for (i = 0; i < cusnum; i++)
            {               
                read = bufread.readLine();
                savecustomerdata(read);
            }          
        
            bufread.close();  
        } catch (FileNotFoundException ex) {  
            ex.printStackTrace();  
        } catch (IOException ex) {  
            ex.printStackTrace();  
        }          
    }
        public void savecustomerdata(String st)
    {
        String[] cinfo = st.split(",");
        cus = cc.add();
        cus.setFname(cinfo[0]);
        cus.setLname(cinfo[1]);
        cus.setSelflight(cinfo[2]);        
        cus.setSeatrow(Integer.valueOf(cinfo[3]));
        cus.setSeatcolumn(Integer.valueOf(cinfo[4]));        
    }

    public void Calculate()
    {
        ms = new MasterSchedule(am,cc);
        tot = ms.calprice();
    } 
        



    public void print()
    {
        try {  
            
            
            File file = new File("hw3out.csv");             
            FileWriter fw = new FileWriter(file);  
            BufferedWriter bw = new BufferedWriter(fw);  
            bw.write("All Airliners,,Total\r\n");
            bw.write(",,"+tot+"\r\n");
        for (Airliners airliner : am.getAm())
        {          
            bw.write("\r\n");
            bw.write(airliner.getAirlinerid()+",,Subtotal"+",");
            for (Flights fl : airliner.getFlight())
                bw.write(","+fl.getId());
            bw.write("\r\n");
            bw.write(",,"+airliner.getFleettot()+",");
            for (Flights fl : airliner.getFlight())           
                bw.write(","+fl.getTot());
            bw.write("\r\n");
        }
        
            
          
            bw.flush();  
            bw.close();  
  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
    }
    

}
