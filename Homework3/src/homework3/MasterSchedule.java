/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework3;
import java.io.*;
import java.util.ArrayList;
/**
 *
 * @author Reyes
 */
public class MasterSchedule {
    private AirlinerManager am;
    private CustomerCatalog cc;
    
    public MasterSchedule(AirlinerManager am, CustomerCatalog cc)
    { 
        this.am = am;
        this.cc = cc;        
    }
    
    
    
     public int calprice()
    {
        Airliners airl = null;
        boolean flag;
        int flighttot=0, airlinertot=0;
        for (Customer cus : cc.getCc())
        {
            flag = false;
            for (Airliners airliner : am.getAm())  
            {
                
                for (Flights fl : airliner.getFlight())
                    if (cus.getSelflight().equals(fl.getId()))
                    {
                       fl.setSeatsavail(cus.getSeatrow(), cus.getSeatcolumn(),1);
                       flag = true;
                       break;
                    }
                if (flag) break;
            }
        }
        
        int tot = 0;
        int[][] seats;
        int[][] seatsavail;
        for (Airliners airliner : am.getAm())
        {
            airlinertot = 0;
            for (Flights fl: airliner.getFlight())
            {
                flighttot = 0;
                seats = fl.getPrice();
                seatsavail = fl.getSeatsavail();
                for (int i = 0; i < 25; i++)
                    for (int j = 0; j < 6; j++)
                        flighttot += seats[i][j] * seatsavail[i][j];
                fl.setTot(flighttot);
               // System.out.println("    Airplane "+airp.getId()+"'s total is: "+ airplanetot);
                airlinertot += flighttot;
            }
            tot += airlinertot;
            airliner.setFleettot(airlinertot);
            //System.out.println("Airliner "+airliner.getAirlinerid()+"'s total is: "+ airlinertot);
        }
       // System.out.println("The total is: "+tot);  
       return tot;
    }    
     
}
