/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework3;
import java.util.ArrayList;
/**
 *
 * @author Reyes
 */
public class CustomerCatalog {
    private ArrayList<Customer> cc;

    public CustomerCatalog() {
        cc = new ArrayList<Customer>();
    }
    

    public ArrayList<Customer> getCc() {
        return cc;
    }

    public void setCc(ArrayList<Customer> cc) {
        this.cc = cc;
    }
    
    public Customer add()
    {
        Customer cus = new Customer();
        cc.add(cus);
        return cus;
    }
}
