package Business.Role;


import Business.GunSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.Role.Role;
import Business.UserAccount.UserAccount;
import Business.*;
import javax.swing.JPanel;
import userinterface.ClinicRole.ClinicWorkAreaJPanel;


public class ManufactorAdminRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, GunSystem system, Network network) {
        return new ClinicWorkAreaJPanel(userProcessContainer,account, organization,enterprise, system.getVaccineSystem(),network);
    }
    
    
    
}
