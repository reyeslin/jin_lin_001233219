/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Role.DistributorAdminRole;
import Business.Role.Role;
import java.util.ArrayList;


public class DistributorEnterprise extends Enterprise{
    public DistributorEnterprise(String name) {
        super(name, Enterprise.EnterpriseType.DistributorEnterprise);
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new DistributorAdminRole());
        return roles;
    }
}
