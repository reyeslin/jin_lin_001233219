/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Gun;

import java.util.ArrayList;


public class BulletDirectory {
    private ArrayList<Bullet>BulletList ;

    public BulletDirectory() {
        BulletList=new ArrayList<>();
    }

    public ArrayList<Bullet> getBulletList() {
        return BulletList;
    }

    public void setBulletList(ArrayList<Bullet> BulletList) {
        this.BulletList = BulletList;
    }

    
     public Bullet createVaccine(String name,Gun gun) {
     for(Bullet bullet:BulletList){
            if(bullet.getName().equals(name))
                return null;
        }
        Bullet bullet = new Bullet();
        bullet.setName(name);
        bullet.setGun(gun);
        BulletList.add(bullet);
        return bullet;
    }
    public void removevaccine(Bullet bullet){
        BulletList.remove(bullet);
    }
    
}
