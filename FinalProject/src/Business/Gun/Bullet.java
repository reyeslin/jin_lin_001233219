/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Gun;


public class Bullet {
     private String Name;
    private static int counter=1;
    private int BulletID;
    private Gun gun;

    public Bullet() {
        BulletID=counter;
        counter++;
        gun=new Gun();
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getBulletID() {
        return BulletID;
    }

    public void setBulletID(int BulletID) {
        this.BulletID = BulletID;
    }

    public Gun getGun() {
        return gun;
    }

    public void setGun(Gun gun) {
        this.gun = gun;
    }

   
     @Override
    public String toString(){
    return Name;
    }
}
