/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Gun;


public class GunInventory {
      private Bullet bullet;
      private Gun gun;
      private int inventory;
       
        public GunInventory() {
           bullet = new Bullet();
        }

    public Bullet getBullet() {
        return bullet;
    }

    public void setBullet(Bullet bullet) {
        this.bullet = bullet;
    }

    public Gun getGun() {
        return gun;
    }

    public void setGun(Gun gun) {
        this.gun = gun;
    }
    
    
        public int getInventory(){
            return inventory;
        }
        public void setNum(int num){
            inventory =num;
        }  
        @Override
        public String toString(){
            return this.bullet.getName();
        }
}
