/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Gun;


public class GunSystem {
    private GunDirectory gunDirectory;
    private BulletDirectory bulletDirectory;

    public GunSystem() {
        gunDirectory = new GunDirectory();
        bulletDirectory = new BulletDirectory();
    }

    public GunDirectory getGunDirectory() {
        return gunDirectory;
    }

    public void setGunDirectory(GunDirectory gunDirectory) {
        this.gunDirectory = gunDirectory;
    }

    public BulletDirectory getBulletDirectory() {
        return bulletDirectory;
    }

    public void setBulletDirectory(BulletDirectory bulletDirectory) {
        this.bulletDirectory = bulletDirectory;
    }

    

}
