
package Business.Organization.DistributorOrganization;


import Business.Organization.Organization;
import Business.Role.DistributorAdminRole;
import Business.Role.Role;
import java.util.ArrayList;

public class DistributeOrganization extends Organization{
   
  
    public DistributeOrganization() {
        super(Type.DistributeOrganization.getValue());
        
    }
    
   
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new DistributorAdminRole());
        return roles;
    }
    
}
