
package Business.Organization.ManufacturerOrganization;


import Business.Organization.Organization;
import Business.Role.ManufacturerAdminRole;
import Business.Role.Role;
import java.util.ArrayList;

public class ManufacturerOrganization extends Organization{

    public ManufacturerOrganization() {
        super(Organization.Type.ManufactorOrganization.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new ManufacturerAdminRole());
        return roles;
    }
     
}