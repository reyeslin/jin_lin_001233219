
package Business.WorkQueue;


public class GunRequest extends WorkRequest{
    private String testResult;

    public String getTestResult() {
        return testResult;
    }

    public void setTestResult(String testResult) {
        this.testResult = testResult;
    }
}
